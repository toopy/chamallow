FROM python:3.8-slim

RUN apt update -y; \
    apt install -y --no-install-recommends openssh-client openssh-server; \
    apt clean; \
    rm -rf /var/lib/apt/lists/*; \
    mkdir -p /root/.ssh /var/run/sshd; \
    echo "root:toor" | chpasswd; \
    echo "PermitRootLogin yes\n" >> /etc/ssh/sshd_config

COPY .       /opt/chamallow
COPY etc/ssh /root/.ssh

WORKDIR /opt/chamallow

RUN pip install --no-cache-dir -e .[zmq]; \
    pip install -e ./examples
