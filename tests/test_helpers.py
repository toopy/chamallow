from functools import partial
from multiprocessing import Process, Queue

import pytest

from chamallow.exceptions import GracefulExit
from chamallow.helpers import import_func, run_func, start_async

from .funcs import func


async def func_process(q, *a, **kw):
    q.put(func(*a, **kw))


async def exiting_process(q):
    assert q.get() == "foo"
    raise GracefulExit
    # not consumed
    assert q.get() == "bar"


def test_import_func():
    assert import_func("tests.funcs.func") == func


@pytest.mark.asyncio
async def test_run_func():
    assert (
        await run_func("tests.funcs.func", args=(1,), kwargs={"b": 2})
    ) == "done with 1 and b=2"


@pytest.mark.asyncio
async def test_run_func_coro():
    assert (
        await run_func("tests.funcs.async_func", args=(1,), kwargs={"b": 2})
    ) == "done with 1 and b=2"


@pytest.mark.asyncio
async def test_run_func_gen():
    assert (await run_func("tests.funcs.gen_func", args=(1,), kwargs={"b": 2})) == [
        1,
        2,
    ]


@pytest.mark.asyncio
async def test_run_func_async_gen():
    assert (
        await run_func("tests.funcs.async_gen_func", args=(1,), kwargs={"b": 2})
    ) == [1, 2]


def test_start_async():
    q = Queue()
    p = Process(target=partial(start_async, func_process, q, 1, b=2))
    p.start()
    assert q.get() == "done with 1 and b=2"
    p.join()


def test_start_async_graceful_exit():
    q = Queue()
    q.put("foo")
    q.put("bar")
    p = Process(target=partial(start_async, exiting_process, q))
    p.start()
    p.join()
    assert q.get() == "bar"
