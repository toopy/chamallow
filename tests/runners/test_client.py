import asyncio
from multiprocessing import Pipe

import orjson
import pytest

from chamallow.messages import Request, RequestKindEnum
from chamallow.runners.client import ClientRunner
from chamallow.settings import settings
from chamallow.states import CoreState

from ..helpers import override_settings


@pytest.mark.asyncio
async def test_client_runner_exit(func):
    p_conn, c_conn = Pipe()

    client = ClientRunner(conn=c_conn)
    client.start()

    for _ in range(5):
        p_conn.send(CoreState.EXITING)
        if client.is_alive():
            await asyncio.sleep(settings.polling_interval * 0.1)

    assert not client.is_alive()


@pytest.mark.asyncio
async def test_client_runner_run(request_run, func, func_id):
    p_conn, c_conn = Pipe()
    client = ClientRunner(conn=c_conn)

    p_conn.send(request_run.to_json())
    p_conn.send(func.to_json())
    p_conn.send(CoreState.EXITING)
    await client._arun()

    assert orjson.loads(await client.cache.get(func_id)) == "done with 1 and b=2"


@pytest.mark.asyncio
async def test_client_runner_result(request_result, func_id):
    p_conn, c_conn = Pipe()
    client = ClientRunner(conn=c_conn)

    # set result
    await client.cache.set(func_id, "foo")

    p_conn.send(request_result.to_json())
    p_conn.send(CoreState.EXITING)
    await client._arun()

    assert p_conn.recv() == "foo"

    # clean cache
    await client.cache.clear()


@pytest.mark.asyncio
async def test_client_runner_no_result(request_result, func_id):
    p_conn, c_conn = Pipe()
    client = ClientRunner(conn=c_conn)

    p_conn.send(request_result.to_json())
    p_conn.send(CoreState.EXITING)
    await client._arun()

    assert orjson.loads(p_conn.recv()) == {"id": func_id, "error": "not-found"}


@pytest.mark.asyncio
async def test_client_runner_not_match(mocker):
    no_match_mock = mocker.patch("chamallow.runners.client.client_backend.not_match")

    request = Request(id="foo", kind=RequestKindEnum.run, port=44000, tags=["foo"])

    p_conn, c_conn = Pipe()
    client = ClientRunner(conn=c_conn)

    p_conn.send(request.to_json())
    p_conn.send(CoreState.EXITING)
    with override_settings("LOCAL", False):
        await client._arun()

    assert no_match_mock.call_args_list == [mocker.call(c_conn, request)]
