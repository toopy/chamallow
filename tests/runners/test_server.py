import asyncio
from multiprocessing import Pipe

import pytest

from chamallow.runners.server import ServerRunner
from chamallow.settings import settings
from chamallow.states import CoreState


@pytest.mark.asyncio
async def test_server_runner_exit(func):
    p_conn, c_conn = Pipe()

    server = ServerRunner(conn=c_conn)
    server.start()

    for _ in range(5):
        p_conn.send(CoreState.EXITING)
        if server.is_alive():
            await asyncio.sleep(settings.polling_interval * 0.1)

    assert not server.is_alive()


def test_server_runner_run(request_run, func):
    p_conn, c_conn = Pipe()
    client_p_conn, client_c_conn = Pipe()

    server = ServerRunner(conn=c_conn, client_conns=(client_c_conn,))
    server.start()

    # trigger run
    p_conn.send(func)

    # check requested
    assert client_p_conn.recv() == request_run.to_json()
    assert client_p_conn.recv() == func.to_json()

    # send exit & join
    p_conn.send(CoreState.EXITING)
    server.join()


def test_server_runner_result(request_result, func, result_req):
    p_conn, c_conn = Pipe()
    client_p_conn, client_c_conn = Pipe()

    server = ServerRunner(conn=c_conn, client_conns=(client_c_conn,))
    server.start()

    # trigger run
    p_conn.send(func)
    for _ in range(2):
        client_p_conn.recv()

    # send result request
    p_conn.send(result_req)
    # check requested
    assert client_p_conn.recv() == request_result.to_json()
    client_p_conn.send(b'"foo"')
    assert p_conn.recv() == "foo"

    # send exit & join
    p_conn.send(CoreState.EXITING)
    server.join()
