import pytest

from chamallow.messages import Func, ResultRequest


@pytest.fixture
def func():
    return Func(
        "tests.funcs.func",
        args=[
            1,
        ],
        kwargs={"b": 2},
        tags=[],
    )


@pytest.fixture
def func_id(func):
    return func.get_hash()


@pytest.fixture
def result_req(func_id):
    return ResultRequest(id=func_id)
