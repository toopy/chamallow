from contextlib import asynccontextmanager
from tempfile import NamedTemporaryFile

import aiofiles
import pytest
import yaml


@pytest.fixture
def yaml_data():
    return {
        "foo": {
            "name": "tests.funcs.func",
            "args": [1],
            "kwargs": {"b": 2},
        }
    }


@pytest.fixture
def yaml_data_w__from():
    return {
        "foo": {"name": "tests.funcs.func", "args": [1], "kwargs": {"b": 2}},
        "bar": {
            "name": "tests.funcs.func",
            "args": [{"_from": "foo"}],
            "depends": ["foo"],
        },
    }


@asynccontextmanager
async def tmp_yaml(data):
    with NamedTemporaryFile() as tmp_f:
        async with aiofiles.open(tmp_f.name, "w") as f:
            await f.write(yaml.safe_dump(data))
            await f.seek(0)
        yield tmp_f.name


@pytest.fixture
@pytest.mark.asyncio
async def yaml_path(yaml_data):
    async with tmp_yaml(yaml_data) as tmp_path:
        yield tmp_path


@pytest.fixture
@pytest.mark.asyncio
async def yaml_path_w__from(yaml_data_w__from):
    async with tmp_yaml(yaml_data_w__from) as tmp_path:
        yield tmp_path
