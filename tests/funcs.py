def func(a, b=None):
    return f"done with {a} and b={b}"


async def async_func(*a, **kw):
    return func(*a, **kw)


def gen_func(a, b=None):
    yield a
    yield b


async def async_gen_func(*a, **kw):
    for x in gen_func(*a, **kw):
        yield x
