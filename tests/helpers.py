from contextlib import contextmanager
from functools import partial
from multiprocessing import Process

from dynaconf.base import Settings

from chamallow.helpers import start_async
from chamallow.settings import settings


@contextmanager
def as_process(*args):
    proc = Process(target=partial(start_async, *args))
    proc.start()
    yield proc
    proc.join()
    assert not proc.exitcode


@contextmanager
def override_settings(key, value):
    old_settings = settings._settings._wrapped
    settings.__dict__.pop(key.lower(), None)
    settings._settings._wrapped = Settings(settings_module=None, **{key: value})
    yield
    settings._settings._wrapped = old_settings
    settings.__dict__.pop(key.lower(), None)
