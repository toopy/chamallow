import pytest

from chamallow.__main__ import run, run_server
from chamallow.messages import ResultRequest


class Args:
    def __init__(self, path, args="", kwargs="{}"):
        self.path = path
        self.args = args
        self.kwargs = kwargs


@pytest.mark.asyncio
async def test_run_server(func_id, mocker, yaml_path_w__from):
    result_mock = mocker.patch(
        "chamallow.decorators.engine.result", return_value="a-result"
    )
    await run_server(yaml_path_w__from)
    assert result_mock.call_args_list == [mocker.call(ResultRequest(id=func_id))]


def test_main(mocker):
    mocker.patch("chamallow.__main__.configure_logging")
    mocker.patch("chamallow.__main__.parse_args", return_value=Args(None))
    engine_start_mock = mocker.patch("chamallow.__main__.engine.start")
    start_async_mock = mocker.patch("chamallow.__main__.start_async")
    run()
    assert engine_start_mock.called
    assert not start_async_mock.called


def test_main_with_path(mocker):
    mocker.patch("chamallow.__main__.configure_logging")
    mocker.patch("chamallow.__main__.parse_args", return_value=Args("demo/flow.yml"))
    engine_start_mock = mocker.patch("chamallow.__main__.engine.start")
    start_async_mock = mocker.patch("chamallow.__main__.start_async")
    run()
    assert engine_start_mock.called
    assert start_async_mock.call_args_list == [
        mocker.call(run_server, "demo/flow.yml", args=[], kwargs={})
    ]


def test_main_with_args(mocker):
    mocker.patch("chamallow.__main__.configure_logging")
    mocker.patch(
        "chamallow.__main__.parse_args",
        return_value=Args("demo/flow.yml", args="foo,bar"),
    )
    engine_start_mock = mocker.patch("chamallow.__main__.engine.start")
    start_async_mock = mocker.patch("chamallow.__main__.start_async")
    run()
    assert engine_start_mock.called
    assert start_async_mock.call_args_list == [
        mocker.call(run_server, "demo/flow.yml", args=["foo", "bar"], kwargs={})
    ]


def test_main_with_kwargs(mocker):
    mocker.patch("chamallow.__main__.configure_logging")
    mocker.patch(
        "chamallow.__main__.parse_args",
        return_value=Args("demo/flow.yml", kwargs='{"foo": "bar"}'),
    )
    engine_start_mock = mocker.patch("chamallow.__main__.engine.start")
    start_async_mock = mocker.patch("chamallow.__main__.start_async")
    run()
    assert engine_start_mock.called
    assert start_async_mock.call_args_list == [
        mocker.call(run_server, "demo/flow.yml", args=[], kwargs={"foo": "bar"})
    ]
