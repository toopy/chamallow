from multiprocessing.connection import Connection

import pytest

from chamallow.core import Engine
from chamallow.messages import ResultRequest
from chamallow.runners import ClientRunner, ServerRunner


@pytest.fixture
def engine():
    f = Engine()
    yield f
    f.stop()


def test_engine_clients(engine):
    assert "clients" not in engine.__dict__
    assert "client_conns" not in engine.__dict__
    assert all(isinstance(c, ClientRunner) for c in engine.clients)
    assert "clients" in engine.__dict__
    assert "client_conns" in engine.__dict__


def test_engine_client_conns(engine):
    assert "client_conns" not in engine.__dict__
    assert all(
        (
            isinstance(c, dict)
            and list(c.keys()) == ["parent", "child"]
            and all(isinstance(p, Connection) for p in c.values())
        )
        for c in engine.client_conns
    )
    assert "client_conns" in engine.__dict__


def test_engine_server(engine):
    assert "client_conns" not in engine.__dict__
    assert "server" not in engine.__dict__
    assert "server_conns" not in engine.__dict__
    assert isinstance(engine.server, ServerRunner)
    assert "client_conns" in engine.__dict__
    assert "server" in engine.__dict__
    assert "server_conns" in engine.__dict__


def test_engine_server_conns(engine):
    assert "server_conns" not in engine.__dict__
    assert all(isinstance(c, Connection) for c in engine.server.client_conns)
    assert isinstance(engine.server.conn, Connection)
    assert "server_conns" in engine.__dict__


def test_engine_result(engine, func):
    engine.start()
    func_id = engine.run(func)
    assert engine.result(ResultRequest(id=func_id)) == "done with 1 and b=2"


def test_engine_result_error(engine, func_id):
    engine.start()
    with pytest.raises(RuntimeError):
        engine.result(ResultRequest(id=func_id))


def test_engine_run(func, func_id):
    assert Engine().run(func) == func_id


def test_engine_start():
    pass


def test_engine_stop():
    pass
