from multiprocessing import Pipe

import orjson
import pytest

from chamallow.backends.process import ProcessClientBackend
from chamallow.states import CoreState


@pytest.mark.asyncio
async def test_ProcessClientBackend_aiter_requests(caplog, request_run):
    p_conn, c_conn = Pipe()

    p_conn.send(request_run.to_json())
    p_conn.send(b'"foo"')
    p_conn.send(orjson.dumps({"a": 1}))
    p_conn.send(CoreState.EXITING)

    assert [f async for f in ProcessClientBackend().aiter_requests(c_conn)] == [
        request_run
    ]
    assert [li.message for li in caplog.records] == [
        "invalid message: b'\"foo\"'",
        "invalid message: b'{\"a\":1}'",
    ]


@pytest.mark.asyncio
async def test_ProcessClientBackend_async_it(func):
    p_conn, c_conn = Pipe()
    p_conn.send(func.to_json())

    result = await ProcessClientBackend().async_it(c_conn, None)
    assert result == "done with 1 and b=2"


@pytest.mark.asyncio
async def test_ProcessClientBackend_not_match():
    with pytest.raises(NotImplementedError):
        await ProcessClientBackend().not_match(None, None)


@pytest.mark.asyncio
async def test_ProcessClientBackend_result():
    p_conn, c_conn = Pipe()
    await ProcessClientBackend().result(c_conn, None, "foo")

    assert p_conn.recv() == "foo"
