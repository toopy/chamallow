from collections import namedtuple
from multiprocessing import Pipe

import pytest

from chamallow.backends.process import ProcessServerBackend
from chamallow.states import ClientState

Runner = namedtuple("Runner", "client_conns")


@pytest.mark.asyncio
async def test_async_it(request_run, func, func_id):
    p_conn, c_conn = Pipe()
    server_backend = ProcessServerBackend()

    await server_backend.async_it(Runner([c_conn]), func)

    assert p_conn.recv() == request_run.to_json()
    assert p_conn.recv() == func.to_json()

    assert server_backend._client_indexes[func_id] == 0


@pytest.mark.asyncio
async def test_async_it_no_client(func):
    server_backend = ProcessServerBackend()
    assert (await server_backend.async_it(Runner([]), func)) == ClientState.NO_CLIENT


@pytest.mark.asyncio
async def test_result(request_result, func_id):
    p_conn, c_conn = Pipe()
    server_backend = ProcessServerBackend()
    server_backend._client_indexes[func_id] = 0

    p_conn.send(b'"foo"')
    await ProcessServerBackend().result(Runner([c_conn]), func_id)

    assert p_conn.recv() == request_result.to_json()
