from functools import partial
from multiprocessing import Pipe

import pytest
import zmq
import zmq.asyncio

from chamallow.backends.zmq.server import (
    MessageIterator,
    ZMQServerBackend,
    bind_pair_socket,
    socket_poller,
)
from chamallow.messages import Request
from chamallow.settings import settings
from chamallow.states import ClientState

from ...helpers import as_process


async def _pair_client(port, helo=False, msg=None, not_match=False):
    socket = zmq.asyncio.Context().socket(zmq.PAIR)
    socket.connect(f"tcp://localhost:{port}")
    if not_match:
        return await socket.send(b"not-match")
    if helo:
        await socket.send(b"helo")
    if not msg:
        msg = await socket.recv()
    await socket.send(b"done" if helo else msg)


async def _port_client(conn):
    socket = zmq.asyncio.Context().socket(zmq.PULL)
    socket.connect(f"tcp://localhost:{settings.connect_port}")
    request = Request.from_json(await socket.recv())
    conn.send(request.port)


@pytest.fixture
def pull_client():
    async def _client():
        socket = zmq.asyncio.Context().socket(zmq.PULL)
        socket.connect(f"tcp://localhost:{settings.connect_port}")
        await socket.recv() == b"foo"

    with as_process(_client) as p:
        yield p


@pytest.mark.asyncio
async def test_bind_pair_socket():
    with bind_pair_socket() as (socket, port):
        with as_process(partial(_pair_client, port)):
            await socket.send(b"foo")
            await socket.recv() == b"foo"


@pytest.mark.asyncio
async def test_socket_poller():
    with bind_pair_socket() as (socket, port):
        with as_process(partial(_pair_client, port)):
            with socket_poller(socket) as poller:
                await socket.send(b"foo")
                socks = dict(await poller.poll(timeout=settings.polling_interval))
                if socket in socks and socks[socket] == zmq.POLLIN:
                    await socket.recv() == b"foo"


@pytest.mark.asyncio
async def test_MessageIterator():
    async def _server(conn):
        with bind_pair_socket() as (socket, port):
            conn.send(port)
            socket.send(b"foo")
            with socket_poller(socket) as poller:
                assert (await MessageIterator(socket, poller).__anext__()) == "foo"

    p_conn, c_conn = Pipe()

    with as_process(partial(_server, c_conn)):
        port = p_conn.recv()
        with as_process(partial(_pair_client, port)):
            pass


@pytest.mark.asyncio
async def test_ZMQServerBackend_socket(pull_client):
    socket = ZMQServerBackend().socket
    await socket.send(b"fo")


@pytest.mark.asyncio
async def test_ZMQServerBackend_close():
    backend = ZMQServerBackend()
    backend.close()
    with pytest.raises(zmq.error.ZMQError):
        await backend.socket.send(b"foo")


@pytest.mark.asyncio
async def test_ZMQServerBackend_async_it(func):
    async def _backend():
        assert (await ZMQServerBackend().async_it(None, func)) == ClientState.DONE

    with as_process(_backend):
        p_conn, c_conn = Pipe()
        with as_process(partial(_port_client, c_conn)):
            port = p_conn.recv()
        with as_process(partial(_pair_client, port, helo=True)):
            pass


@pytest.mark.asyncio
async def test_ZMQServerBackend_async_it_not_match(func):
    async def _backend():
        assert (await ZMQServerBackend().async_it(None, func)) == ClientState.NOT_MATCH

    with as_process(_backend):
        p_conn, c_conn = Pipe()
        with as_process(partial(_port_client, c_conn)):
            port = p_conn.recv()
        with as_process(partial(_pair_client, port, not_match=True)):
            pass


def test_ZMQServerBackend_result(func_id):
    async def _backend():
        assert await ZMQServerBackend().result(None, func_id) == "foo"

    with as_process(_backend):
        p_conn, c_conn = Pipe()
        with as_process(partial(_port_client, c_conn)):
            port = p_conn.recv()
        with as_process(partial(_pair_client, port, msg=b'"foo"')):
            pass
