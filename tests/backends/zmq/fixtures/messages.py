import pytest

from chamallow.messages import Request, RequestKindEnum


@pytest.fixture
def request_run(func_id):
    return Request(id=func_id, kind=RequestKindEnum.run, port=44000, tags=[])


@pytest.fixture
def request_result(func_id):
    return Request(id=func_id, kind=RequestKindEnum.result, port=44000, tags=[])
