from contextlib import asynccontextmanager
from functools import partial
from multiprocessing import Pipe

import pytest
import zmq
import zmq.asyncio

from chamallow.backends.zmq.client import (
    ZMQClientBackend,
    connect_pair_socket,
    connect_socket,
    fetch_run_data,
    socket_poller,
)
from chamallow.messages import Func
from chamallow.settings import settings
from chamallow.states import CoreState

from ...helpers import as_process


@pytest.fixture
def pair_server():
    async def _server():
        socket = zmq.asyncio.Context().socket(zmq.PAIR)
        socket.bind("tcp://*:44000")
        msg = None
        while msg not in [b"done", b"foo", b"not-match"]:
            msg = await socket.recv()
            if msg not in [b"done", b"helo", b"not-match"]:
                await socket.send(msg)

    with as_process(_server) as p:
        yield p


@pytest.fixture
def push_server(request_run):
    async def _server():
        socket = zmq.asyncio.Context().socket(zmq.PUSH)
        socket.bind(f"tcp://*:{settings.connect_port}")
        await socket.send(request_run.to_json())

    with as_process(_server) as p:
        yield p


def test_fetch_run_data(func, pair_server):
    async def _client():
        async with connect_pair_socket(44000) as socket:
            await socket.send(func.to_json())
            async with fetch_run_data(socket) as run_data:
                assert Func(**run_data) == func

    with as_process(_client):
        pass


@pytest.mark.asyncio
async def test_connect_pair_socket(pair_server):
    async def _client():
        async with connect_pair_socket(44000) as socket:
            await socket.send(b"foo")
            assert (await socket.recv()) == b"foo"

    with as_process(_client):
        pass


@pytest.mark.asyncio
async def test_connect_socket(request_run, push_server):
    async def _client():
        with connect_socket() as socket:
            assert (await socket.recv()) == request_run.to_json()

    with as_process(_client):
        pass


def test_socket_poller(request_run, push_server):
    async def _client():
        msg = None
        with connect_socket() as socket, socket_poller(socket) as poller:
            while not msg:
                socks = dict(await poller.poll(timeout=settings.polling_interval))
                if socket in socks and socks[socket] == zmq.POLLIN:
                    msg = await socket.recv()
        assert msg == request_run.to_json()

    with as_process(_client):
        pass


def test_ZMQClientBackend_aiter_requests(request_run, push_server):
    async def _client(p_conn, c_conn):
        async for request in ZMQClientBackend().aiter_requests(c_conn):
            if request:
                p_conn.send(CoreState.EXITING)
        assert request == request_run

    with as_process(_client, *Pipe()):
        pass


@pytest.mark.asyncio
async def test_ZMQClientBackend_async_it(request_run, func, func_id, mocker):
    @asynccontextmanager
    async def fake_fetch_run_data(run_data, *a):
        yield run_data

    mocker.patch(
        "chamallow.backends.zmq.client.fetch_run_data",
        side_effect=partial(fake_fetch_run_data, func._asdict()),
    )

    assert (
        await ZMQClientBackend().async_it(None, request_run)
    ) == "done with 1 and b=2"


@pytest.mark.asyncio
async def test_ZMQClientBackend_not_match(request_run, pair_server):
    assert not await ZMQClientBackend().not_match(None, request_run)


@pytest.mark.asyncio
async def test_ZMQClientBackend_result(request_run, pair_server):
    assert not await ZMQClientBackend().result(None, request_run, b"done")
