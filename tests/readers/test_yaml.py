import pytest

from chamallow.readers.yaml import YamlReader, depends_from


@pytest.mark.parametrize(
    "expected, values",
    (
        ([], []),
        ([], ["a"]),
        (["b"], [{"_from": "b"}]),
        (["b"], ["a", {"_from": "b"}]),
        (["b"], [{"_from": "b"}, "c"]),
        (["b"], ["a", {"_from": "b"}, "c"]),
    ),
)
def test_depends_from(expected, values):
    assert depends_from(values) == expected


@pytest.mark.asyncio
async def test_yaml(yaml_data, yaml_path):
    assert (await YamlReader(yaml_path).yaml) == yaml_data


@pytest.mark.asyncio
async def test_yaml_async_iterator(yaml_data, yaml_path):
    yaml_reader = YamlReader(yaml_path).__aiter__()
    assert (await yaml_reader.__anext__()) == yaml_data
    with pytest.raises(StopAsyncIteration):
        await yaml_reader.__anext__()
