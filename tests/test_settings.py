import pytest

from chamallow.settings import settings

from .helpers import override_settings


@pytest.mark.parametrize("debug", (0, 1, False, True))
def test_settings_bool(debug):
    with override_settings("DEBUG", str(debug)):
        assert settings.debug == bool(debug)
    assert not settings.debug


@pytest.mark.parametrize("debug", ("invalid", ""))
def test_settings_bad_bool(debug):
    with override_settings("DEBUG", debug):
        assert not settings.debug


@pytest.mark.parametrize("interval", ("0", "4"))
def test_settings_int(interval):
    with override_settings("POLLING_INTERVAL", interval):
        assert settings.polling_interval == int(interval)
    assert settings.polling_interval == 10


@pytest.mark.parametrize("interval", ("bad", "False", ""))
def test_settings_bad_int(interval):
    with override_settings("POLLING_INTERVAL", interval):
        with pytest.raises(ValueError):
            assert settings.polling_interval


def test_settings_str():
    with override_settings("ADDRESS", "0.0.0.0"):
        assert settings.address == "0.0.0.0"
    assert settings.address == "localhost"


@pytest.mark.parametrize(
    "expected, tags",
    (
        (set(), ""),
        (set(("foo",)), "foo,"),
        (
            set(
                (
                    "foo",
                    "bar",
                )
            ),
            "foo, bar, ",
        ),
    ),
)
def test_settings_tags(expected, tags):
    with override_settings("TAGS", tags):
        assert settings.tags == expected
    assert not settings.tags
