from chamallow.decorators import LazyResult, flow
from chamallow.messages import Func, ResultRequest


@flow()
def func(a, b=None):
    pass


def test_LazyResult_result(mocker):
    result_mock = mocker.patch(
        "chamallow.decorators.engine.result", return_value="a-lazy-result"
    )
    assert LazyResult("an-id").result() == "a-lazy-result"
    assert result_mock.call_args_list == [mocker.call(ResultRequest(id="an-id"))]


def test_LazyResult_result_error(mocker):
    result_mock = mocker.patch(
        "chamallow.decorators.engine.result", side_effect=RuntimeError
    )
    sys_exit_mock = mocker.patch("sys.exit")
    assert not LazyResult("an-id").result()
    assert result_mock.call_args_list == [mocker.call(ResultRequest(id="an-id"))]
    assert sys_exit_mock.call_args_list == [mocker.call(1)]


def test_flow():
    lazy_result = func(1, b=2)
    assert isinstance(lazy_result, LazyResult)
    assert (
        lazy_result.func_id
        == Func(
            "tests.test_decorators.func", args=[1], kwargs={"b": 2}, tags=()
        ).get_hash()
    )
