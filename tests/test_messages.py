import pytest

from chamallow.exceptions import MessageException
from chamallow.messages import (
    Error,
    ErrorEnum,
    Func,
    Request,
    RequestKindEnum,
    ResultRequest,
)


def test_ErrorEnum_from_value():
    assert ErrorEnum.from_value("not-found") == ErrorEnum.not_found


def test_ErrorEnum_from_value_error():
    with pytest.raises(MessageException):
        assert ErrorEnum.from_value("invalid")


def test_Error_from_json():
    assert Error.from_json(b'{"id":"an-id","error":"not-found"}') == Error(
        id="an-id",
        error=ErrorEnum.not_found,
    )


def test_Error_to_json():
    assert (
        Error.to_json(
            Error(
                id="an-id",
                error=ErrorEnum.not_found,
            )
        )
        == b'{"id":"an-id","error":"not-found"}'
    )


@pytest.mark.parametrize(
    "kind_enum, value",
    (
        (RequestKindEnum.result, "result"),
        (RequestKindEnum.run, "run"),
    ),
)
def test_RequestKindEnum_from_value(kind_enum, value):
    assert RequestKindEnum.from_value(value) == kind_enum


def test_RequestKindEnum_from_value_error():
    with pytest.raises(MessageException):
        assert RequestKindEnum.from_value("invalid")


def test_Request_from_json():
    assert Request.from_json(
        b'{"id":"an-id","kind":"result","port":123,"tags":[]}'
    ) == Request(id="an-id", kind=RequestKindEnum.result, port=123, tags=[])


def test_Request_to_json():
    assert (
        Request.to_json(
            Request(id="an-id", kind=RequestKindEnum.result, port=123, tags=[])
        )
        == b'{"id":"an-id","kind":"result","port":123,"tags":[]}'
    )


def test_Func_from_json():
    assert Func.from_json(
        b'{"name":"a-name","args":[1,"a"],"kwargs":{"foo":"bar"},"tags":[]}'
    ) == Func(name="a-name", args=[1, "a"], kwargs={"foo": "bar"}, tags=[])


def test_Func_get_hash():
    assert Func(
        name="a-name", args=[1, "a"], kwargs={"foo": "bar"}, tags=[]
    ).get_hash() == (
        "2cb40595e075d302cea9a7db97ab88fab4c1654e5facc26c26305b4ec308e539f"
        "3e8d0569b6addded351b8b4c0e2a15454f72399ad654a783080e97cde82c214"
    )


def test_Func_to_json():
    assert (
        Func.to_json(Func(name="a-name", args=[1, "a"], kwargs={"foo": "bar"}, tags=[]))
        == b'{"name":"a-name","args":[1,"a"],"kwargs":{"foo":"bar"},"tags":[]}'
    )


def test_ResultRequest_from_json():
    assert ResultRequest.from_json(b'{"id":"an-id"}') == ResultRequest(id="an-id")


def test_ResultRequest_to_json():
    assert ResultRequest(id="an-id").to_json() == b'{"id":"an-id"}'
