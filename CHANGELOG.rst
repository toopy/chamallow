CHANGELOG
=========

0.1.2 (unreleased)
------------------

- Nothing changed yet.


0.1.1 (2021-07-14)
------------------

- Handle conditional funcs
- Allow result override
- Handle options from cli or env when reading yaml


0.1.0 (2021-07-12)
------------------

- First implementation
