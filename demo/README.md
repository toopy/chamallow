# Chamallow Examples

TODO

## Setup

```bash
(venv)$ pip install -e demo
```

## Run The chamallow-demo Command

```bash
(venv)$ chamallow-demo
```

## Run The demo.yml

```bash
(venv)$ chamallow demo/flow.yml
```

## Run From Func Path

```bash
(venv)$ chamallow chamallow_demo.funcs.run -a '!@*'
```
