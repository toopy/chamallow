import json

import aiofiles

from chamallow import flow


@flow()
async def fetch(foo, bar=None):
    return {
        "foo": foo,
        "bar": bar,
    }


@flow()
async def to_csv(data):
    yield ";".join(list(data.keys()))
    yield ";".join(list(data.values()))


@flow()
def to_json(data=None):
    return json.dumps(data, indent=2)


@flow(tags=["foo"])
async def write(csv, json_):
    async with aiofiles.open("simple.csv", mode="wb") as f:
        for li in csv:
            await f.write(li.encode() + b"\n")
    async with aiofiles.open("simple.json", mode="wb") as f:
        await f.write(json_.encode() + b"\n")
    return {"result": "ok"}


def run(foo, bar="YYY"):
    a = fetch(foo, bar=bar)
    b = write(csv=to_csv(data=a), json_=to_json(data=a))
    return b.result()
