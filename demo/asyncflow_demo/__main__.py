from chamallow import engine
from chamallow.logging import configure_logging

from .funcs import run


def _main():
    configure_logging()
    engine.start()
    assert run("xxx") == {"result": "ok"}
    engine.stop()


if __name__ == "__main__":
    _main()
